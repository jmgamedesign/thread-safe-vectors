package vectors;

public final class ComputingVector2D
{
    /*
    ComputingVector2Ds are not thread safe. They exist to make
    vector math more efficient than by making new ImmutableVector2D
    objects every time a calculation is needed, but their use
    should be handled with care. They are intended to be short-lived
    objects used in small bursts. Any violation of their intended
    use ought to be carefully considered.

    This is also why their X and Y components are private.
    They cannot be used as mutable 2D vectors because in practice they aren't.
    They are simply an intermediate stage that ImmutableVector2D objects
    endure when calculations need to be performed on them.
     */

    //#region statics

    private static final double NO_DIVISION_BY_ZERO_ALLOWED = .0001;

    //#endregion

    //#region member fields

    private volatile double x;
    private volatile double y;

    //#endregion

    //#region constructors

    public ComputingVector2D()
    {
        x = 0;
        y = 0;
    }

    public ComputingVector2D(ImmutableVector2D _v)
    {
        x = _v.x;
        y = _v.y;
    }

    public ComputingVector2D(ComputingVector2D _v)
    {
        x = _v.x;
        y = _v.y;
    }

    //#endregion

    //#region vector math

    public final ComputingVector2D setX(double _x)
    {
        x = _x;

        return this;
    }

    public final ComputingVector2D setY(double _y)
    {
        y = _y;

        return this;
    }

    public final ComputingVector2D setXY(double _x, double _y)
    {
        x = _x;
        y = _y;

        return this;
    }

    public final ComputingVector2D setZero()
    {
        x = 0;
        y = 0;

        return this;
    }

    public final ComputingVector2D add(ComputingVector2D _v)
    {
        double tempX = x + _v.x;
        double tempY = y + _v.y;

        x = tempX;
        y = tempY;

        return this;
    }

    public final ComputingVector2D add(ImmutableVector2D _v)
    {
        double tempX = x + _v.x;
        double tempY = y + _v.y;

        x = tempX;
        y = tempY;

        return this;
    }

    public final ComputingVector2D sub(ComputingVector2D _v)
    {
        double tempX = x - _v.x;
        double tempY = y - _v.y;

        x = tempX;
        y = tempY;

        return this;
    }

    public final ComputingVector2D sub(ImmutableVector2D _v)
    {
        double tempX = x - _v.x;
        double tempY = y - _v.y;

        x = tempX;
        y = tempY;

        return this;
    }

    public final ComputingVector2D mul(double _scalar)
    {
        double tempX = x * _scalar;
        double tempY = y * _scalar;

        x = tempX;
        y = tempY;

        return this;
    }

    public final ComputingVector2D mul(ComputingVector2D _v)
    {
        double tempX = x * _v.x;
        double tempY = y * _v.y;

        x = tempX;
        y = tempY;

        return this;
    }

    public final ComputingVector2D mul(ImmutableVector2D _v)
    {
        double tempX = x * _v.x;
        double tempY = y * _v.y;

        x = tempX;
        y = tempY;

        return this;
    }

    public final ComputingVector2D div(double _divisor)
    {
        if (_divisor == 0.0)
        {
            _divisor = NO_DIVISION_BY_ZERO_ALLOWED;
        }

        double tempX = x / _divisor;
        double tempY = y / _divisor;

        x = tempX;
        y = tempY;

        return this;
    }

    public final ComputingVector2D div(ComputingVector2D _v)
    {
        double vx = _v.x;
        double vy = _v.y;

        if (vx == 0.0)
        {
            vx = NO_DIVISION_BY_ZERO_ALLOWED;
        }

        if (vy == 0.0)
        {
            vy = NO_DIVISION_BY_ZERO_ALLOWED;
        }

        double tempX = x / vx;
        double tempY = y / vy;

        x = tempX;
        y = tempY;

        return this;
    }

    public final ComputingVector2D div(ImmutableVector2D _v)
    {
        double vx = _v.x;
        double vy = _v.y;

        if (vx == 0.0)
        {
            vx = NO_DIVISION_BY_ZERO_ALLOWED;
        }

        if (vy == 0.0)
        {
            vy = NO_DIVISION_BY_ZERO_ALLOWED;
        }

        double tempX = x / vx;
        double tempY = y / vy;

        x = tempX;
        y = tempY;

        return this;
    }

    public final double dot(ComputingVector2D _v)
    {
        return x * _v.x + y * _v.y;
    }

    public final double dot(ImmutableVector2D _v)
    {
        return x * _v.x + y * _v.y;
    }

    public final double cross(ComputingVector2D _v)
    {
        return x * _v.y - y * _v.x;
    }

    public final double cross(ImmutableVector2D _v)
    {
        return x * _v.y - y * _v.x;
    }

    public final double len2()
    {
        //x * x + y * y
        return dot(this);
    }

    public final double len()
    {
        return Math.sqrt(len2());
    }

    public final double dst2(ComputingVector2D _v)
    {
        double newX = x - _v.x;
        double newY = y - _v.y;

        return newX * newX + newY * newY;
    }

    public final double dst2(ImmutableVector2D _v)
    {
        double newX = x - _v.x;
        double newY = y - _v.y;

        return newX * newX + newY * newY;
    }

    public final double dst(ComputingVector2D _v)
    {
        return Math.sqrt(dst2(_v));
    }

    public final double dst(ImmutableVector2D _v)
    {
        return Math.sqrt(dst2(_v));
    }

    public final ComputingVector2D unitVector()
    {
        double len = len();

        //If no unit vector is possible this prevents useless division.
        if(len == 0.0)
        {
            return this;
        }

        return div(len);
    }

    public final ComputingVector2D unitNormalVector()
    {
        unitVector();

        double tempX = x;
        double tempY = y;

        x = tempY;
        y = -tempX;

        return this;
    }

    public final ComputingVector2D setLength2(double _len2)
    {
        return setLength(Math.sqrt(_len2));
    }

    public final ComputingVector2D setLength(double _len)
    {
        double oldLen = len();

        //Because if the length is zero there's no reasonable way to extend it in any direction. This just stops useless multiplication.
        if (oldLen == 0.0)
        {
            return this;
        }

        return mul(_len / oldLen);
    }

    public final ComputingVector2D clamp2(double _min2, double _max2)
    {
        double oldLen2 = len2();

        if (oldLen2 < _min2)
        {
            return setLength2(_min2);
        }

        if (oldLen2 > _max2)
        {
            return setLength2(_max2);
        }

        return this;
    }

    public final ComputingVector2D clamp(double _min, double _max)
    {
        double oldLen2 = len2();

        if (oldLen2 < (_min * _min))
        {
            return setLength(_min);
        }

        if (oldLen2 > (_max * _max))
        {
            return setLength(_max);
        }

        return this;
    }

    public final ComputingVector2D limit2(double _len2)
    {
        return clamp2(0, _len2);
    }

    public final ComputingVector2D limit(double _len)
    {
        return clamp(0, _len);
    }

    //#endregion

    public final ImmutableVector2D endCompute()
    {
        //This method returns the result of the vector computations back to an ImmutableVector2D object.
        return new ImmutableVector2D(x, y);
    }

    @Override
    public final String toString()
    {
        return "Mutable: (" + x + " , " + y + ")";
    }
}