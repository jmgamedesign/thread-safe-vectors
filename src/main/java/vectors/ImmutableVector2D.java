package vectors;

public final class ImmutableVector2D
{
    public static final ImmutableVector2D X_VECTOR = new ImmutableVector2D(1, 0);
    public static final ImmutableVector2D Y_VECTOR = new ImmutableVector2D(0, 1);
    public static final ImmutableVector2D ZERO_VECTOR = new ImmutableVector2D();

    public final double x;
    public final double y;

    public ImmutableVector2D()
    {
        x = 0.0;
        y = 0.0;
    }

    public ImmutableVector2D(double _x, double _y)
    {
        x = _x;
        y = _y;
    }

    //#region immutable vector math

    /*
    These methods do not have overloads for ComputingVector2D objects
    because the entire point of this library is to provide an API
    for working with vectors that is heavily resistant to race conditions.
    ComputingVector2D objects allow for efficient vector math,
    but they are also thread unsafe. Therefore it is preferred
    that they be short-lived objects whenever possible. To encourage this
    they are designed to only be easy to use in short bursts.
    Any violation of their intended use should be taken with care.
     */

    public final double dot(ImmutableVector2D _v)
    {
        return x * _v.x + y * _v.y;
    }

    public final double cross(ImmutableVector2D _v)
    {
        return x * _v.y - y * _v.x;
    }

    public final double len2()
    {
        //x * x + y * y
        return dot(this);
    }

    public final double len()
    {
        return Math.sqrt(len2());
    }

    public final double dst2(ImmutableVector2D _v)
    {
        double newX = x - _v.x;
        double newY = y - _v.y;

        return newX * newX + newY * newY;
    }

    public final double dst(ImmutableVector2D _v)
    {
        return Math.sqrt(dst2(_v));
    }

    //#endregion

    public final ComputingVector2D beginCompute()
    {
        //This created a mutable object from this ImmutableVector2D object that can be used for vector calculations.
        return new ComputingVector2D(this);
    }

    @Override
    public final String toString()
    {
        return "Immutable: (" + x + " , " + y + ")";
    }
}